#!/usr/bin/ruby
require "nyaplot"

require_relative "../lib/performance"
require_relative "../lib/fourier"

inputs = {
  :target_func => lambda {|x| Math::cos(2*x) + Math::sin(5*x)},
  :steps => 64
}

func_args = inputs[:steps].times.map do |index|
  2 * Math::PI  / inputs[:steps] * index
end

func_values = func_args.map do |arg|
  inputs[:target_func].call arg
end

four = log_time_for "discrete fourier transform calculation" do
  discrete_fourier func_values
end

puts "Discrete results valid: #{seq_equal func_values, rev_discrete_fourier(four)}"
discrete_plot = Nyaplot::Plot::new
ampl = discrete_plot.add :line, func_args, ampl(four)
ampl.color 'red'
discrete_plot.add :line, func_args, func_values
phase = discrete_plot.add :line, func_args, phase(four)
phase.color 'green'
discrete_plot.export_html "../outputs/discrete.html"

puts

four = log_time_for "fast fourier trainsform calculation" do
  fast_fourier func_values
end

puts "Discrete results valid: #{seq_equal func_values, rev_fast_fourier(four)}"
fast_plot = Nyaplot::Plot::new
ampl = fast_plot.add :line, func_args, ampl(four)
ampl.color 'red'
fast_plot.add :line, func_args, func_values
phase = fast_plot.add :line, func_args, phase(four)
phase.color 'green'
fast_plot.export_html "../outputs/fast.html"

#!/usr/bin/ruby

require 'free-image'

require_relative '../lib/img-transform'
require_relative '../lib/performance'

b = FreeImage::Bitmap.open('sample.png')
intensity = 256.times.map { |_| 0 } 

inputs = [{ :name => 'f_of_i', :combinator => lambda {|i| clamp_intensity(i - 30)} },
          { :name => 'g_of_i', :combinator => lambda {|i| clamp_intensity(i + 30)} } ]

log_time_for 'convetion to grayscale' do
  grayscale(b)
  b.save('../outputs/grayscale.png', :png)
end

log_time_for 'sample intensity histogram building' do
  plot_img_intensity b, 'initial'
end

inputs.each do |input|
  img = FreeImage::Bitmap.open '../outputs/grayscale.png'
  color = FreeImage::RGBQuad.new

  log_time_for "#{input[:name]} transform" do
    apply_intensity_transform img, input[:combinator]
    img.save "../outputs/#{input[:name]}", :png
  end

  log_time_for "#{input[:name]} intensity histogram building" do
    plot_img_intensity img, input[:name]
  end
end

require_relative "fourier"
require_relative "common"

def fast_convolute(x, y)
  rev_fast_fourier [x, y].map { |seq| fast_fourier seq }
        .transpose
        .map { |row| row.reduce(:*) }
end

def fast_correlate(x, y)
    rev_fast_fourier [fast_fourier(x).map(&:conj), fast_fourier(y)]
                      .transpose
                      .map { |row| row.reduce(:*) }
end

def convolute(x, y)
  x.length.times.map do |m|
    sum = x.length.times.map do |h|
      x[h] * y[loop_index(m - h, x.length)]
    end
    sum.reduce(:+) / x.length.to_f
  end
end

def correlate(x, y)
    x.length.times.map do |m|
      sum = x.length.times.map do |h|
        x[h] * y[loop_index(m + h, x.length)]
      end
      sum.reduce(:+) / x.length.to_f
    end
end


def loop_index(index, size)
    if (index < 0) then
      return index + size
    elsif (index >= size) then
      return index - size
    end
    index
end

def phase(seq)
  seq.map { |item| Math::atan(item.imag / item.real) }
end

def ampl(seq)
  seq.map { |item| item.abs }
end

def seq_equal(seq1, seq2)
  deltas = seq1.map.with_index do |item, index|
    item - seq2[index]
  end
  
  deltas
    .map {|item| item.abs}
    .all? { |abs_val| abs_val < 0.0001 }
end

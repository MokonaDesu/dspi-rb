def fast_walsh(seq)
  return seq.map {|x| x / seq.length } if (seq.length == 1)

  a = []
  b = []
  
  (seq.length / 2).times do |index|
    a.push seq[index] + seq[index + seq.length / 2]
    b.push seq[index] - seq[index + seq.length / 2]
  end

  fast_walsh(a) + fast_walsh(b)
end

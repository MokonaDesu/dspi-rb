require 'free-image'
require 'nyaplot'
 
def plot_img_intensity(bitmap, name)
  intensity = 256.times.map {|_| 0}
  args = 256.times.map {|i| i}

  bitmap.width.times do |x|
    bitmap.height.times do |y|      
      color = bitmap.pixel_color x, y
      curr = pixel_intensity(bitmap, x, y)
      intensity[curr] = intensity[curr] + 1
    end
  end
   

  intensity_plot = Nyaplot::Plot::new
  intensity_plot.add :line, args, intensity
  intensity_plot.export_html "../outputs/#{name}.html"
end

def grayscale(b)
  color = FreeImage::RGBQuad.new

  b.width.times do |x|
    b.height.times do |y|
      current_color = b.pixel_color x, y
      color[:red] = color[:green] = color[:blue] = (current_color[:red] + current_color[:green] + current_color[:blue]) / 3
      b.set_pixel_color(x, y, color) 
    end
  end
end

def pixel_intensity(b, x, y)
  color = b.pixel_color x, y
  (color[:red] + color[:green] + color[:blue]) / 3
end

def color_from_intensity(color, intensity)
  color[:red] = intensity
  color[:green] = intensity
  color[:blue] = intensity
  color
end

def apply_intensity_transform(bitmap, transformer)
  color = FreeImage::RGBQuad.new
  
  bitmap.width.times do |x|
    bitmap.height.times do |y|
      intensity = pixel_intensity(bitmap, x, y)
      new_color = color_from_intensity(color, transformer.call(intensity))
      bitmap.set_pixel_color x, y, new_color
    end
  end
end

def clamp_intensity(value)
  if value > 255 then
    return 255
  end

  if value < 0 then
    return 0
  end

  value
end


def log_time_for method_group
    start = Time.now
    val = yield
    finish = Time.now
    delta = (finish - start) * 1000
    if delta > 5 then
      delta = delta.to_i
    else
      delta = (delta * 100).to_i / 100.0
    end
    puts "Executed #{method_group} in #{delta} ms."
    val
end

require_relative "common"

def discrete_fourier(seq)
  seq.length.times.map.with_index do |_, k|
    seq
      .map
      .with_index {|item, m| item * Math::E ** Complex(0, -2 * Math::PI * k * m / seq.length)}
      .reduce(:+)
  end
end

def rev_discrete_fourier(seq)
  seq.length.times.map do |k|
    (1 / seq.length.to_f) * seq.map
      .with_index { |item, m| item * Math::E ** Complex(0, 2 * Math::PI * k * m / seq.length) }
      .reduce(:+)
  end
end

def fast_fourier(seq)
  _fast_fourier_inner seq, false
end

def rev_fast_fourier(seq)
  _fast_fourier_inner(seq, true).map { |val| val / seq.length }
end

def _fast_fourier_inner (seq, reverse)
  return seq if seq.length == 1
  
  b_even = _fast_fourier_inner(seq.find_all.with_index { |_, index| index.even? }, reverse)
  b_odd = _fast_fourier_inner(seq.find_all.with_index { |_, index| index.odd? }, reverse)
  
  omega_n = Math::cos(2 * Math::PI / seq.length) + (reverse ? -1 : 1) * Complex(0,  Math::sin(2 * Math::PI / seq.length))

  result = []
  
  omega = 1
  (seq.length / 2).times do |index|
    result[index] = b_even[index] + omega * b_odd[index]
    result[index + seq.length / 2] = b_even[index] - omega * b_odd[index]
    omega *= omega_n
  end
  
  result
end

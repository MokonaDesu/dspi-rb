#!/usr/bin/ruby

require "nyaplot"

require_relative "../lib/convolute-correlate"
require_relative "../lib/performance"

inputs = {
    :functions => {
        :x => lambda { |x| Math::cos(2*x) },
        :y => lambda { |x| Math::sin(5*x) }
    },
    :steps => 32
}

function_arguments = inputs[:steps].times.map {|index| 2 * Math::PI / inputs[:steps] * index}    
function_values = inputs[:functions].keys.map do |key|
  [key, function_arguments.map { |arg| inputs[:functions][key].call(arg) }]  
end.to_h

conv = log_time_for "X*Y convolution" do
  convolute function_values[:x], function_values[:y]
end

fast_conv = log_time_for "Fast fourier X*Y convolution" do
  fast_convolute function_values[:x], function_values[:y]
end

basic_conv_plot = Nyaplot::Plot::new
conv_line = basic_conv_plot.add :line, function_arguments, ampl(conv.map {|x| x * 20})
conv_line.color "red"
conv_line = basic_conv_plot.add :line, function_arguments, ampl(fast_conv)
conv_line.color "green"

func_line = basic_conv_plot.add :line, function_arguments, function_values[:x].map {|x| x / 10**15}
func_line.color 'purple'
func_line = basic_conv_plot.add :line, function_arguments, function_values[:y].map {|x| x / 10**15}
func_line.color 'magenta'

basic_conv_plot.export_html "../outputs/convolution.html"

correlation = log_time_for "X, Y correlation" do
  correlate function_values[:x], function_values[:y]
end

fast_correlation = log_time_for "Fast X, Y correlation" do
  fast_correlate function_values[:x], function_values[:y]
end

basic_corr_plot = Nyaplot::Plot::new
corr_line = basic_corr_plot.add :line, function_arguments, ampl(correlation.map {|x| x * 20})
corr_line.color "red"
corr_line = basic_corr_plot.add :line, function_arguments, ampl(fast_correlation)
corr_line.color "green"

func_line = basic_corr_plot.add :line, function_arguments, function_values[:x].map {|x| x / 10**15}
func_line.color 'purple'
func_line = basic_corr_plot.add :line, function_arguments, function_values[:y].map {|x| x / 10**15}
func_line.color 'magenta'

basic_corr_plot.export_html "../outputs/correlation.html"

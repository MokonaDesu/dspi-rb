#!/usr/bin/ruby
require "nyaplot"

require_relative "../lib/performance"
require_relative "../lib/walsh"

inputs = {
  :target_func => lambda {|x| Math::cos(2*x) + Math::sin(5*x)},
  :steps => 256
}

func_args = inputs[:steps].times.map do |index|
  2 * Math::PI  / inputs[:steps] * index
end

func_values = func_args.map do |arg|
  inputs[:target_func].call arg
end

walsh_seq = log_time_for "walsh transform computation" do
  fast_walsh func_values
end

rev_walsh_seq = log_time_for "reverse transform computation" do
  fast_walsh walsh_seq
end

func_values = func_values.map {|x| x * 20}
#rev_walsh_seq = rev_walsh_seq.map {|x| x / 13}

walsh_plot = Nyaplot::Plot::new
walsh_line = walsh_plot.add :line, func_args, walsh_seq
walsh_line.color 'red'
walsh_plot.add :line, func_args, func_values
rev_walsh_line = walsh_plot.add :line, func_args, rev_walsh_seq
rev_walsh_line.color 'magenta'
walsh_plot.export_html "../outputs/walsh.html"


